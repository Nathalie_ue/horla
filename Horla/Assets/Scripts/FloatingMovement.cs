﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace GPPR
{
    //script für fliegende Objekte
    public class FloatingMovement : MonoBehaviour
    {
        //wie schnell sich die Objekte bewegen
        [SerializeField]
        private float floatSpeed;

        //wie hoch sie fliegen
        [SerializeField]
        private float floatHight;

        //variabel für originale position der Objekte auf der y achse
        private float originalYposition;

        void Start()
        {
            //original poition auf der y achse wird in der variabel gespeichert
            originalYposition = gameObject.transform.localPosition.y;
        }
        
        void FixedUpdate()
        {
            //die position des objectes wird mit einer neuen + bewegung überschrieben
            transform.localPosition = new Vector3(transform.localPosition.x, originalYposition + ((float)Mathf.Sin(Time.realtimeSinceStartup * floatSpeed) * floatHight), transform.localPosition.z);
        }
    }
}
