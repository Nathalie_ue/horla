﻿using UnityEngine;
using System.Collections;

namespace GPPR
{
    //Script um zum Schlafzimmer bzw vom Schlafzimmer in den Flur zukommen
    public class Teleporter : MonoBehaviour
    {
        //Kamera, auf die sich bezogen wird
        [SerializeField]
        private Camera mainCamera;

        //Variabel für das InteractWithDoor script
        [SerializeField]
        private InteractWithDoor interactWithDoor;

        //Objektname der Tür
        [SerializeField]
        private string doorObjectName;

        //Gameobjekt mit dem Text
        [SerializeField]
        private GameObject interactableText;

        //maximal Distanz für den Raycast
        [SerializeField]
        private float maxDistance = 50f;

        //FixedUpdate wird je nach framerate einmal, mehrmals oder garnicht per frame aufgerufen
        void FixedUpdate()
        {
            //Ray der von der Camera aus nach vone ausgeht und alles innerhalb der maxDistance ausgibt 
            RaycastHit whatIHit;
            if (Physics.Raycast(mainCamera.transform.position, mainCamera.transform.forward, out whatIHit, maxDistance))
            {
                //Wenn der Objektname der Tür getroffen wird, wird das Text Gameobject aktiviert
                if (whatIHit.transform.name == doorObjectName)
                {
                    interactableText.SetActive(true);

                    //Wenn der linke Mausbutton geklickt wird, wird die methode aus dem interactWithDoor script aufgerufen
                    if (Input.GetMouseButtonDown(0))
                    {
                        interactWithDoor.InteractableDoor();
                    }
                }
                else
                {
                    //Wenn der Objektname der Tür nicht getroffen wird, wird das Text Gameobject deaktiviert
                    interactableText.SetActive(false);
                }
            }
        }
    }
}
