﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace GPPR
{
    //Script, dass die Animationen abspielt
    public class PlayAnimation : MonoBehaviour
    {
        //Der animator mit den verwendeten animationen
        [SerializeField]
        private Animator levelTransition;

        //Die zeit, die die animation braucht, um vollständig abgespielt zu werden
        [SerializeField]
        private float transitionTime = 1.30f;

        //Der Name der animation, die abgespielt werden soll
        [SerializeField]
        private string animationName;

        //Das Datum, dass angezeigt werden soll
        [SerializeField]
        private string date;

        //Textobjekt, auf das sich bezogen wird
        [SerializeField]
        private TextMeshProUGUI textMesh;

        //Bei Awake wird der Text im Textobjekt mit dem im Inspektor 
        //zugewiesenden Datum ersetzt und die Coroutine startet
        private void Awake()
        {
            textMesh.text = date;
            StartCoroutine(AnimationStart());
        }

        //Die Coroutine
        IEnumerator AnimationStart()
        {
            //Animation wird abgespielt
            levelTransition.Play(animationName);

            //Zeit, die Animation benötigt um vollständig abgespielt zu werden
            yield return new WaitForSeconds(transitionTime);
        }
    }
}
