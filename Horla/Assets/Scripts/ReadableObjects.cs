﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GPPR
{
    //Script für die Objekte, die nach Interaktion einen Text anzeigen 
    public class ReadableObjects : MonoBehaviour
    {
        //Der gezeigte Text
        [SerializeField]
        private GameObject shownText;

        //Kamera, auf die sich bezogen wird
        [SerializeField]
        private Camera mainCamera;

        //Der name des Briefobjektes
        [SerializeField]
        private string letterObjectName;

        //Text, der beim anschauen des Objektes gezeigt wird
        [SerializeField]
        private GameObject interactableText;

        //Der Text des Erzählers
        [SerializeField]
        private GameObject narrativeText;

        //CoverUp, damit das Objekt nicht mehr erkannt wird
        [SerializeField]
        private GameObject coverUp;

        //maximal Distanz für den Raycast
        [SerializeField]
        private float maxDistance = 10f;

        //Spieler, auf den sich bezogen wird
        [SerializeField]
        private GameObject player;

        //Beim start wird das CoverUp ausgeschaltet
        private void Start()
        {
            coverUp.SetActive(false);
        }

        //FixedUpdate wird je nach framerate einmal, mehrmals oder garnicht per frame aufgerufen
        private void FixedUpdate()
        {
            //Ray der von der Camera aus nach vone ausgeht und alles innerhalb der maxDistance ausgibt
            RaycastHit whatIHit;
            if (Physics.Raycast(mainCamera.transform.position, mainCamera.transform.forward, out whatIHit, maxDistance))
            {
                //Wenn der Briefobjektname getroffen wird, wird das Text Gameobject aktiviert
                if (whatIHit.transform.name == letterObjectName)
                {
                    interactableText.SetActive(true);

                    //Wenn der linke Mausbutton geklickt wird, wird das coverUp und der gezeigte Text aktiviert
                    if (Input.GetMouseButtonDown(0))
                    {
                        coverUp.SetActive(true);
                        shownText.SetActive(true);
                    }
                }
                else
                {
                    //Wenn der Objektname nicht getroffen wird, wird das Text Gameobject deaktiviert
                    interactableText.SetActive(false);
                }
            }
        }

        private void Update()
        {
            //Wenn Enter gedrückt wird und der gezeigte Text aktiv ist, wird der gezeigte Text deaktiviert und die Coroutine startet
            if (Input.GetKeyDown(KeyCode.Return) && shownText.activeInHierarchy)
            {
                shownText.SetActive(false);

                StartCoroutine(LoadText());
            }
        }

        //Die Coroutine
        IEnumerator LoadText()
        {
            //Der Erzähler Text wird aktiviert
            narrativeText.SetActive(true);

            //Zeit, die der Spieler zum lesen des Textes hat
            yield return new WaitForSeconds(5f);

            //Der Text des Erzählers und das CoverUp wird deaktiviert
            narrativeText.SetActive(false);
            coverUp.SetActive(false);
        }
    }
}
