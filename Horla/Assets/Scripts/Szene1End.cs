﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

namespace GPPR
{
    //Sript für das Szenenende von Szene 1
    public class Szene1End : MonoBehaviour
    {
        //Animator in dem sich die Animationen befinden
        [SerializeField]
        private Animator transition;

        //die zeit, die die Animation braucht
        [SerializeField]
        private float transitionTime = 1f;

        //Kamera, auf die sich bezogen wird
        [SerializeField]
        private Camera mainCamera;

        //Der name des Bettobjektes
        [SerializeField]
        private string bedObjectName;

        //Text, der beim anschauen des Objektes gezeigt wird
        [SerializeField]
        private GameObject interactableText;

        //Name der Animation
        [SerializeField]
        private string animationName;

        //FixedUpdate wird je nach framerate einmal, mehrmals oder garnicht per frame aufgerufen
        private void FixedUpdate()
        {
            //Ray der von der Camera aus nach vone ausgeht und alles innerhalb der maxDistance ausgibt
            RaycastHit whatIHit;
            if (Physics.Raycast(mainCamera.transform.position, mainCamera.transform.forward, out whatIHit))
            {
                //Wenn der Objektname getroffen wird, wird das Text Gameobject aktiviert
                if (whatIHit.transform.name == bedObjectName)
                {
                    interactableText.SetActive(true);

                    //Wenn der linke Mausbutton geklickt wird, wird die Coroutine gestartet
                    if (Input.GetMouseButtonDown(0))
                    {
                        StartCoroutine(LoadLevel(SceneManager.GetActiveScene().buildIndex + 1));
                    }
                }
                else
                {
                    //Wenn der Objektname nicht getroffen wird, wird das Text Gameobject deaktiviert
                    interactableText.SetActive(false);
                }
            }
        }

        //Die Coroutine
        IEnumerator LoadLevel(int levelIndex)
        {
            //Der Text wird deaktiviert
            interactableText.SetActive(false);

            //Animation wird abgespielt
            transition.Play(animationName);

            //Zeit, die Animation benötigt, um vollständig abgespielt zu werden
            yield return new WaitForSeconds(transitionTime);

            //Nächste Szene im Index wird geladen
            SceneManager.LoadScene(levelIndex);
        }
    }
}
