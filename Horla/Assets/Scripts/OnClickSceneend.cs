﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

namespace GPPR
{
    //Script für alle Szenenenden, die der Spieler selbst bestätigen muss
    public class OnClickSceneend : MonoBehaviour
    {
        //Der animator mit den verwendeten animationen
        [SerializeField]
        private Animator transition;

        //Die zeit, die die animation braucht, um vollständig abgespielt zu werden
        [SerializeField]
        private float transitionTime = 1f;

        //der Name der animation, die abgespielt werden soll
        [SerializeField]
        private string animationName;

        //die zeit, die der Text angezeigt wird
        [SerializeField]
        private float textShownTime = 4f;

        //Der gezeigte Text
        [SerializeField]
        private GameObject shownText;

        //Kamera, auf die sich bezogen wird
        [SerializeField]
        private Camera mainCamera;

        //Der name des objektes
        [SerializeField]
        private string objectName;

        //Text, der beim anschauen des Objektes gezeigt wird
        [SerializeField]
        private GameObject interactableText;
        
        //Der Text des Erzählers
        [SerializeField]
        private GameObject narrativeText;

        //CoverUp, damit das Objekt nicht mehr erkannt wird
        [SerializeField]
        private GameObject coverUp;

        //maximal Distanz für den Raycast
        [SerializeField]
        private float maxDistance = 10f;

        //Beim start wird das CoverUp ausgeschaltet
        private void Start()
        {
            coverUp.SetActive(false);
        }

        //FixedUpdate wird je nach framerate einmal, mehrmals oder garnicht per frame aufgerufen
        private void FixedUpdate()
        {
            //Ray der von der Camera aus nach vone ausgeht und alles innerhalb der maxDistance ausgibt
            RaycastHit whatIHit;
            if (Physics.Raycast(mainCamera.transform.position, mainCamera.transform.forward, out whatIHit, maxDistance))
            {
                //Wenn der Objektname getroffen wird, wird das Text Gameobject aktiviert
                if (whatIHit.transform.name == objectName)
                {
                    interactableText.SetActive(true);

                    //Wenn der linke Mausbutton geklickt wird, wird das coverUp und der gezeigte Text aktiviert
                    if (Input.GetMouseButtonDown(0))
                    {
                        coverUp.SetActive(true);
                        shownText.SetActive(true);
                    }
                }
                else
                {
                    //Wenn der Objektname nicht getroffen wird, wird das Text Gameobject deaktiviert
                    interactableText.SetActive(false);
                }
            }
        }

        private void Update()
        {
            //Wenn Enter gedrückt wird und der gezeigte Text aktiv ist, wird der gezeigte Text deaktiviert und die Coroutine startet
            if (Input.GetKeyDown(KeyCode.Return) && shownText.activeInHierarchy)
            {
                shownText.SetActive(false);

                StartCoroutine(LoadLevel(SceneManager.GetActiveScene().buildIndex + 1));
            }
        }

        //Die Coroutine
        IEnumerator LoadLevel(int levelIndex)
        {
            //Der Erzähler Text wird aktiviert
            narrativeText.SetActive(true);

            //Zeit, die der Spieler zum lesen des Textes hat
            yield return new WaitForSeconds(textShownTime);

            //Animation wird abgespielt
            transition.Play(animationName);

            //Zeit, die Animation benötigt um vollständig abgespielt zu werden
            yield return new WaitForSeconds(transitionTime);

            //Nächste Szene im Index wird geladen
            SceneManager.LoadScene(levelIndex);
        }
    }
}
