﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

namespace GPPR
{
    //Script das die Animationen der Credits abspielt
    public class Credits : MonoBehaviour
    {
        //Variabeln, die im Inspektor den Szenennamen vom MainMenu bekommt
        [SerializeField]
        private string sceneName;

        //Variabeln mit den Namen der Animationen
        [SerializeField]
        private string animationStartName;

        [SerializeField]
        private string animationEndName;

        [SerializeField]
        private string creditAnimationName;

        //Der Animator mit den verwendeten Animationen
        [SerializeField]
        private Animator transition;

        //Zeit, die gewartet wird, bevor das Script weitergeht
        [SerializeField]
        private float transitionTime;

        //Zeit, die die Animationen brauchen um vollständig abgespielt zu werden
        [SerializeField]
        private float creditsTime;

        private void Start()
        {
            //Maus beim Start der Szene verstecken
            Cursor.lockState = CursorLockMode.Locked;

            //Beim starten dieser Szene fängt die Coroutine an
            StartCoroutine(LoadLevel());            
        }

        private void Update()
        {
            //Wenn Escape gedrückt wird, wird die Szene geladen
            if (Input.GetKey(KeyCode.Escape))
            {
                SceneManager.LoadScene(sceneName);
            }
        }

        //Die Coroutine
        IEnumerator LoadLevel()
        {
            //Text wird eingeblendet
            transition.Play(animationStartName);

            //Lesezeit für Spieler
            yield return new WaitForSeconds(transitionTime);

            //Text wird ausgeblendet
            transition.Play(animationEndName);

            //Warten bevor nächste Animation anfängt
            yield return new WaitForSeconds(1f);

            //Credits werden abgespielt
            transition.Play(creditAnimationName);

            //Warten bis die Credits vollständig gezeigt wurden
            yield return new WaitForSeconds(creditsTime);

            //Neue Szene wird geladen
            SceneManager.LoadScene(sceneName);
        }
    }
}
