﻿using UnityEngine;
using System.Collections;

namespace GPPR
{
    //Script für die Texte innerhalb des Spieles
    public class ShowText : MonoBehaviour
    {
        //Text, auf den sich bezogen wird
        [SerializeField]
        private GameObject texts;

        //Trigger, der den Text auslöst
        [SerializeField]
        private GameObject textTrigger;
        
        //Spieler, auf den sich bezogen wird
        [SerializeField]
        private GameObject player;

        //Wenn ein Trigger betreten wird
        private void OnTriggerEnter(Collider other)
        {
            //Der Text wird aktiviert
            texts.SetActive(true);

            //Die Bewegung des Spielers deaktiviert
            player.GetComponent<PlayerMovement>().enabled = false;

            //Der Collider des Trigger deaktiviert
            GetComponent<BoxCollider>().enabled = false; 
        }
        
        private void Update()
        {
            //Wenn der linke Mausbutton geklickt wird
            if (Input.GetMouseButtonDown(0))
            {
                //Die Bewegung des Spielers wird aktiviert
                player.GetComponent<PlayerMovement>().enabled = true;

                //Der Text wird deaktiviert
                texts.SetActive(false);                
            }
        }
    }
}
