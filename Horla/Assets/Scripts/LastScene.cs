﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

namespace GPPR
{
    //Script für den letzten Moment der letzten Szene
    public class LastScene : MonoBehaviour
    {
        //Der animator mit den verwendeten animationen
        [SerializeField]
        private Animator transition;

        //der Name der animation, die abgespielt werden soll
        [SerializeField]
        private string animationName;

        //Die zeit, die die animation braucht, um vollständig abgespielt zu werden
        [SerializeField]
        private float transitionTime = 1f;

        //Kamera, auf die sich bezogen wird
        [SerializeField]
        private Camera mainCamera;

        //Der Text, wenn objekte anzündbar sind
        [SerializeField]
        private GameObject setFireText;

        //Der name des objektes
        [SerializeField]
        private string objectName;

        //maximal Distanz für den Raycast
        [SerializeField]
        private float maxDistance = 10f;

        //Spieler, auf den sich bezogen wird
        [SerializeField]
        private GameObject player;

        //Das Feuer, auf das sich bezogen wird
        [SerializeField]
        private GameObject fire;

        //CoverUp, damit das Objekt nicht mehr erkannt wird
        [SerializeField]
        private GameObject coverUp;

        //Bei start wird das feuer, das CoverUp und der animator des players ausgeschaltet
        private void Start()
        {
            fire.SetActive(false);
            coverUp.SetActive(false);
            player.GetComponent<Animator>().enabled = false;
        }

        //FixedUpdate wird je nach framerate einmal, mehrmals oder garnicht per frame aufgerufen
        private void FixedUpdate()
        {
            //Ray der von der Camera aus nach vone ausgeht und alles innerhalb der maxDistance ausgibt 
            RaycastHit whatIHit;
            if (Physics.Raycast(mainCamera.transform.position, mainCamera.transform.forward, out whatIHit, maxDistance))
            {
                //Wenn der Objektname getroffen wird, wird das Text Gameobject und der animator des players aktiviert
                if (whatIHit.transform.name == objectName)
                {
                    player.GetComponent<Animator>().enabled = true;
                    setFireText.SetActive(true);

                    //Wenn der linke Mausbutton geklickt wird, wird das feuer und CoverUp aktiviert 
                    //und die coroutine startet
                    if (Input.GetMouseButtonDown(0))
                    {
                        fire.SetActive(true);
                        coverUp.SetActive(true);

                        StartCoroutine(LoadLevel(SceneManager.GetActiveScene().buildIndex + 1));
                    }
                }
                else
                {
                    //Wenn der Objektname nicht getroffen wird, wird das Text Gameobject deaktiviert
                    setFireText.SetActive(false);
                }
            }
        }

        //Die Coroutine
        IEnumerator LoadLevel(int levelIndex)
        {
            //Warten bis das Feuer anfängt
            yield return new WaitForSeconds(3f);
            
            //Animation spielen
            transition.Play(animationName);

            //Warten bis die animation fertig ist
            yield return new WaitForSeconds(transitionTime);

            //Nächste Szene im index wird geladen
            SceneManager.LoadScene(levelIndex);
        }
    }
}
