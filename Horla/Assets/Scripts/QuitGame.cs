﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace GPPR
{
    //Script um das Spiel zu beenden
    public class QuitGame : MonoBehaviour
    {
        public void Quit()
        {
            //Funktion für den Quit Button um das Spiel zu beenden
            Application.Quit();

            Debug.Log("Quit");
        }
    }
}
