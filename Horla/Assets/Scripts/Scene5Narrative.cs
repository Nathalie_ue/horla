﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

namespace GPPR
{
    //Script für Szene 5a
    public class Scene5Narrative : MonoBehaviour
    {
        //die zeit, die die Animation braucht
        [SerializeField]
        private float transitionTime = 1f;

        //Animator in dem sich die Animationen befinden
        [SerializeField]
        private Animator transition;

        //Name der Animation
        [SerializeField]
        private string animationName;

        //Spieler, auf den sich bezogen wird
        [SerializeField]
        private GameObject player;
        
        //Array in dem sich die 9 Textobjekte befinden
        [SerializeField]
        private GameObject[] narrativeText = new GameObject[9]; 
        
        //Variabeln für welche Texte aktiv sein sollen
        private int activateNext = 1;
        private int active = 0;

        
        void Start()
        {
            //Bewegung des Spielers wird ausgeschaltet
            player.GetComponent<PlayerMovement>().enabled = false;

            //Der erste Text steht von Anfang an da
            narrativeText[0].SetActive(true);            
        }

        void Update()
        {

            //Wenn der linke Mausbutton geklickt wird und der Wert in activeNext kleiner
            //als die Array länge ist
            if (Input.GetMouseButtonDown(0) && activateNext < narrativeText.Length)
            {
                //Der nächste Text wird aktiviert und der aktuel aktive deaktiviert
                narrativeText[activateNext].SetActive(true);
                narrativeText[active].SetActive(false);

                //Active und activateNext werden um eins erhöht
                activateNext++;
                active++;
            }
            
            //Wenn der aktive Text der letzte Text des arrays ist, wird die Coroutine gestartet
            if (activateNext == narrativeText.Length)
            {
                StartCoroutine(LoadLevel(SceneManager.GetActiveScene().buildIndex + 1));
            }
        }

        //Die Coroutine
        IEnumerator LoadLevel(int levelIndex)
        {
            //Zeit, die der Spieler zum lesen des Textes hat
            yield return new WaitForSeconds(3f);

            //Animation wird abgespielt
            transition.Play(animationName);

            //Zeit, die Animation benötigt um vollständig abgespielt zu werden
            yield return new WaitForSeconds(transitionTime);

            //Nächste Szene im Index wird geladen
            SceneManager.LoadScene(levelIndex);
        }
    }
}