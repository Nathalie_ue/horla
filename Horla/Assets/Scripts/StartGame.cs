﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

namespace GPPR
{
    //Script, dass das Spiel startet
    public class StartGame : MonoBehaviour
    {
        //Animator in dem sich die Animationen befinden
        [SerializeField]
        private Animator gameStart;

        //die zeit, die die Animation braucht
        [SerializeField]
        private float transitionTime = 1.30f;

        void Start()
        {
            //Animation wird abgespielt
            gameStart.Play("Title_fadeIn");
        }

        //public Methode wird von einem anderen Script aus gerufen
        public void LoadNextLevel()
        {
            //Nimmt die aktive Szene und addiert 1 drauf um die nächste Szene
            //aus den Build settings zu laden
            StartCoroutine(LoadLevel(SceneManager.GetActiveScene().buildIndex + 1));
        }

        //Die Coroutine
        IEnumerator LoadLevel(int levelIndex)
        {
            //Animation wird abgespielt
            gameStart.Play("Game_Start");

            //Zeit, die die Animation braucht um vollständig abgespielt zu werden
            yield return new WaitForSeconds(transitionTime);

            //Nächste Szene im Index wird geladen
            SceneManager.LoadScene(levelIndex);
        }
    }
}
