﻿using UnityEngine;
using System.Collections;

namespace GPPR
{
    //Script für das Hauptmenü
    public class MenuManager : MonoBehaviour
    {
        //Kamera, auf die sich bezogen wird
        [SerializeField]
        private Camera mainCamera;

        //Variabel für das QuitGame script
        [SerializeField]
        private QuitGame quitGame;

        //Variabel für das StartGame script
        [SerializeField]
        private StartGame startGame;

        //maximal Distanz für den Raycast
        [SerializeField]
        private float maxDistance = 10f;

        //FixedUpdate wird je nach framerate einmal, mehrmals oder garnicht per frame aufgerufen
        void FixedUpdate()
        {
            //Ray der von der Camera aus nach vone ausgeht und alles innerhalb der maxDistance ausgibt 
            RaycastHit whatIHit;
            if (Physics.Raycast(mainCamera.transform.position, mainCamera.transform.forward, out whatIHit, maxDistance))
            {
                //Schauen, ob das getroffende Objekt "Start" heißt
                if (whatIHit.transform.name == "Start")
                {
                    //Wenn der linke Mausbutton geklickt wird, wird die methode aus dem startGame script aufgerufen 
                    if (Input.GetMouseButtonDown(0))
                    {
                        startGame.LoadNextLevel();
                    }
                } //Schauen, ob das getroffende Objekt "Quit" heißt
                else if (whatIHit.transform.name == "Quit")
                {
                    //Wenn der linke Mausbutton geklickt wird, wird die methode aus dem quitGame script aufgerufen
                    if (Input.GetMouseButtonDown(0))
                    {
                        quitGame.Quit();
                    }
                }
            }
        }
    }
}
