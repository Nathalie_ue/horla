﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GPPR
{
    //Script für Szenen die automatisch mit dem aktivieren eines Textes enden
    public class TextSceneend : MonoBehaviour
    {
        //Animator in dem sich die Animationen befinden
        [SerializeField]
        private Animator transition;

        //Name der Animation
        [SerializeField]
        private string animationName;

        //die zeit, die die Animation braucht
        [SerializeField]
        private float transitionTime = 1f;

        //die zeit, die der Text angezeigt wird
        [SerializeField]
        private float textShownTime = 4f;

        //Gameobjekt mit dem Text
        [SerializeField]
        private GameObject shownText;

        //Wenn der Trigger betreten wird, wird der Text angezeigt und die Coroutine startet
        private void OnTriggerEnter(Collider other)
        {
            shownText.SetActive(true);

            StartCoroutine(LoadLevel(SceneManager.GetActiveScene().buildIndex + 1));
        }
        
        //Die Coroutine
        IEnumerator LoadLevel(int levelIndex)
        {
            //Zeit, die der Spieler zum lesen des Textes hat
            yield return new WaitForSeconds(textShownTime);

            //Animation wird abgespielt
            transition.Play(animationName);

            //Zeit, die Animation benötigt um vollständig abgespielt zu werden
            yield return new WaitForSeconds(transitionTime);

            //Nächste Szene im Index wird geladen
            SceneManager.LoadScene(levelIndex);
        }
    }
}
