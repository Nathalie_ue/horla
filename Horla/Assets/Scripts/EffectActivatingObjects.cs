﻿using UnityEngine;
using System.Collections;

namespace GPPR
{
    //Script für die Feuer Elemente der Szene 5b
    public class EffectActivatingObjects : MonoBehaviour
    {
        //Kamera, auf die sich bezogen wird
        [SerializeField]
        private Camera mainCamera;

        //Der name des objektes
        [SerializeField]
        private string objectName;

        //Text, der gezeigt wird, wenn man ein Objekt anschaut
        [SerializeField]
        private GameObject interactableText;

        //Text des Erzählers
        [SerializeField]
        private GameObject narrativeText;

        //CoverUp, damit das Objekt nicht mehr erkannt wird
        [SerializeField]
        private GameObject coverUp;

        //Das Feuer, auf das sich bezogen wird
        [SerializeField]
        private GameObject fire;

        //maximal Distanz für den Raycast
        private float maxDistance = 10f;

        //Bei start wird das feuer und das CoverUp ausgeschaltet
        private void Start()
        {
            coverUp.SetActive(false);
            fire.SetActive(false);
        }

        //FixedUpdate wird je nach framerate einmal, mehrmals oder garnicht per frame aufgerufen
        private void FixedUpdate()
        {
            //Ray der von der Camera aus nach vone ausgeht und alles innerhalb der maxDistance ausgibt 
            RaycastHit whatIHit;
            if (Physics.Raycast(mainCamera.transform.position, mainCamera.transform.forward, out whatIHit, maxDistance))
            {
                //Wenn der Objektname getroffen wird, wird das Text Gameobject aktiviert
                if (whatIHit.transform.name == objectName)
                {
                    interactableText.SetActive(true);

                    //Wenn der linke Mausbutton geklickt wird, wird das coverUp aktiviert und die Coroutine gestartet
                    if (Input.GetMouseButtonDown(0))
                    {
                        coverUp.SetActive(true);
                        StartCoroutine(LoadText());
                    }
                }
                else
                {
                    //Wenn der Objektname nicht getroffen wird, wird das Text Gameobject deaktiviert
                    interactableText.SetActive(false);
                }
            }
        }

        //Die Coroutine
        IEnumerator LoadText()
        {
            //Das Feuer wird aktiviert
            fire.SetActive(true);

            //Der narrative Text wird aktiviert
            narrativeText.SetActive(true);

            //Wartezeit, damit die Spieler den Text lesen können
            yield return new WaitForSeconds(5f);

            //Der narrative Text wird deaktiviert
            narrativeText.SetActive(false);
        }
    }
}
