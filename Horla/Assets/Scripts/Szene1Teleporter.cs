﻿using UnityEngine;
using System.Collections;

namespace GPPR
{
    //Script um in Szene 1 zum Schlafzimmer bzw vom Schlafzimmer in den Flur zukommen
    public class Szene1Teleporter : MonoBehaviour
    {
        //Kamera, auf die sich bezogen wird
        [SerializeField]
        private Camera mainCamera;

        //Variabel für das InteractWithDoor script
        [SerializeField]
        private InteractWithDoor interactWithDoor;

        //Objektname der Tür
        [SerializeField]
        private string doorObjectName;

        //Gameobjekt mit dem Text, wenn man die Tür anschaut
        [SerializeField]
        private GameObject interactableText;

        //Gameobjekt mit dem Text, wenn man das Schlafzimmer nicht betreten kann
        [SerializeField]
        private GameObject lockedDoorText;

        //maximal Distanz für den Raycast
        [SerializeField]
        private float maxDistance = 10f;

        //Gameobjekt mit dem Trigger im Garten
        [SerializeField]
        private GameObject gardenTrigger;

        //Variabel die true oder false ausgibt, ob der Trigger aktiv ist
        private bool triggerIsActive;

        //FixedUpdate wird je nach framerate einmal, mehrmals oder garnicht per frame aufgerufen
        void FixedUpdate()
        {
            //BoxCollider des Triggers wird dem bool zugewiesen
            //wenn der Collider aktiv ist, wird true zurückgegeben, wenn nicht dann false
            triggerIsActive = gardenTrigger.GetComponent<BoxCollider>().enabled;

            //Ray der von der Kamera aus nach vone ausgeht und alles innerhalb der maxDistance ausgibt 
            RaycastHit whatIHit;
            if (Physics.Raycast(mainCamera.transform.position, mainCamera.transform.forward, out whatIHit, maxDistance))
            {
                //Wenn der Objektname der Tür getroffen wird
                if (whatIHit.transform.name == doorObjectName)
                {
                    //Wenn der Trigger aktiv ist, wird der Text für die geschlossende Tür aktiviert
                    if (triggerIsActive)
                    {
                        lockedDoorText.SetActive(true);
                    }
                    else //Wenn der Trigger nicht aktiv ist, wird der Text für die nicht verschlossende Tür aktiviert
                    {
                        interactableText.SetActive(true);

                        //Wenn der linke Mausbutton geklickt wird, wird die methode aus dem interactWithDoor script aufgerufen
                        if (Input.GetMouseButtonDown(0))
                        {
                            interactWithDoor.InteractableDoor();
                        }
                    }
                }
                else
                {
                    //Wenn der Objektname der Tür nicht getroffen wird, werden die Text Gameobject deaktiviert
                    lockedDoorText.SetActive(false);
                    interactableText.SetActive(false);
                }
            }
        }
    }
}