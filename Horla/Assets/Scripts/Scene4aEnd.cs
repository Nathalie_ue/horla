﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

namespace GPPR
{
    //Script für das Szenenende vpn Szene 4a
    public class Scene4aEnd : MonoBehaviour
    {
        //Animator in dem sich die Animationen befinden
        [SerializeField]
        private Animator transition;

        //Name der Animation
        [SerializeField]
        private string animationName;

        //die zeit, die die Animation braucht
        [SerializeField]
        private float transitionTime = 1f;

        //Kamera, auf die sich bezogen wird
        [SerializeField]
        private Camera mainCamera;

        //maximal Distanz für den Raycast
        [SerializeField]
        private float maxDistance = 10f;

        //Der name des objektes
        [SerializeField]
        private string objectName;

        //Text, der beim anschauen des Objektes gezeigt wird
        [SerializeField]
        private GameObject interactableText;

        //Der Text des Erzählers
        [SerializeField]
        private GameObject narrativeText;

        //CoverUp, damit das Objekt nicht mehr erkannt wird
        [SerializeField]
        private GameObject coverUp;

        //Beim start wird das CoverUp ausgeschaltet
        private void Start()
        {
            coverUp.SetActive(false);
        }

        //FixedUpdate wird je nach framerate einmal, mehrmals oder garnicht per frame aufgerufen
        private void FixedUpdate()
        {
            //Ray der von der Camera aus nach vone ausgeht und alles innerhalb der maxDistance ausgibt
            RaycastHit whatIHit;
            if (Physics.Raycast(mainCamera.transform.position, mainCamera.transform.forward, out whatIHit, maxDistance))
            {
                //Wenn der Objektname getroffen wird, wird das Text Gameobject aktiviert
                if (whatIHit.transform.name == objectName)
                {
                    interactableText.SetActive(true);

                    //Wenn der linke Mausbutton geklickt wird, wird das coverUp und der narrative Text aktiviert und die Coroutine gestartet
                    if (Input.GetMouseButtonDown(0))
                    {
                        coverUp.SetActive(true);
                        narrativeText.SetActive(true);

                        StartCoroutine(LoadLevel(SceneManager.GetActiveScene().buildIndex + 1));
                    }
                }
                else
                {
                    //Wenn der Objektname nicht getroffen wird, wird das Text Gameobject deaktiviert
                    interactableText.SetActive(false);
                }
            }
        }

        //Die Coroutine
        IEnumerator LoadLevel(int levelIndex)
        {
            //Zeit, die der Spieler zum lesen des Textes hat
            yield return new WaitForSeconds(4f);

            //Der Erzähler Text wird deaktiviert
            narrativeText.SetActive(false);

            //Animation wird abgespielt
            transition.Play(animationName);

            //Zeit, die Animation benötigt um vollständig abgespielt zu werden
            yield return new WaitForSeconds(transitionTime);

            //Nächste Szene im Index wird geladen
            SceneManager.LoadScene(levelIndex);
        }
    }
}
