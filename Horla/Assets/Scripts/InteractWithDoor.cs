﻿using UnityEngine;
using System.Collections;

namespace GPPR
{
    //Script für die Schlafzimmertür
    public class InteractWithDoor : MonoBehaviour
    {
        //Ort an den der Spieler soll
        [SerializeField]
        private Transform teleportTarget;

        //Spieler, auf den sich bezogen wird
        [SerializeField]
        private GameObject player;

        //Methode, die die Position und Rotation des Player Objektes 
        //auf die Position und rotation des target setzt
        public void InteractableDoor()
        {
            player.transform.position = teleportTarget.transform.position;
            player.transform.rotation = teleportTarget.transform.rotation;
        }        
    }
}
